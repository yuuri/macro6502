module Asm where

import Data.Word
import Text.Printf

data Instruction
    = Nul Nullary
    | Un Unary AddrMode
    | Jmp Jump Byte

data Nullary
    = BRK | CLC | {-CLD | -} CLI | CLV | INX | INY | NOP | PHA | PHP | PLA | PLP
    | RTI | RTS | SEC | {-SED | -} SEI | TAX | TAY | TSX | TXA | TXS | TYA 
    deriving Show

data Unary
    = ADC | AND | ASL | BIT | CMP | CPX | CPY | DEC | DEX | DEY | EOR | INC | LDA
    | LDX | LDY | LSR | ORA | ROL | ROR | SBC | STA | STX | STY 
    deriving Show

data Jump   = BCC | BCS | BEQ | BMI | BNE | BPL | BVC | BVS | JMP | JSR
    deriving Show

type Line = Word16

instance Show Command where
    show (Nul nul) = show nul
    show (Un un arg) = printf "%s %s" (show un) (show arg)
    show (Jmp jmp arg) = printf "%s %s" (show jmp) (show arg1)

--TODO: limit addressing modes for different instructions
data AddrMode
    = Accumulator
    | Immediate Byte
    | ZeroPage Byte
    | ZeroPageX Byte
    | Relative Int8 
    | Absolute Word
    | Indirect Word
    | IndirectX Byte
    | IndirectY Byte

instance Show Arg where
    show Accumulator = "A"
    show (Immediate val) = '#' : show val
    show (ZeroPage addr) = '$' : showHex addr
    show (ZeroPageX addr) = printf "$%s,x" (showHex addr)
    show (Relative off) = '*' : (if off >= 0 then ('+':) else id) show off
    show (Absolute addr) = '$' : showHex addr
    show (Indirect addr) = "($%s)" (showHex addr)
    show (IndirectX addr) = "($%s,X)" (showHex addr)
    show (IndirectY addr) = "($%s),Y" (showHex addr)

type Byte = Word8
type Word = Word16

approxTicks :: Instruction -> Int
approxTicks _ = undefined